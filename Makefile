.PHONY: init
init: \
    docker-down \
    docker-up \
	postgres_setup \
	clickhouse_setup

.PHONY: docker-down
docker-down:
	docker compose down

.PHONY: docker-up
docker-up:
	docker compose up --build -d
	sleep 5

.PHONY: postgres_setup
postgres_setup:
	docker compose exec -it postgres-data psql -U postgres -q -f /app/postgres/init.sql

.PHONY: clickhouse_setup
clickhouse_setup:
	docker compose exec -it clickhouse bash -c "clickhouse-client --multiquery < /app/clickhouse/init.sql"
