CREATE TABLE IF NOT EXISTS stats
(
    id UInt32,
    datetime Datetime,
    user_id UInt32,
    event_type String,
    visits UInt32,
    views UInt32,
    clicks UInt32
)
ENGINE = MergeTree()
ORDER BY id;

INSERT INTO stats (id, datetime, user_id, event_type, visits, views, clicks)
SELECT
	number as id,
    now() - randUniform(1, 1000000) as datetime,
    abs(round(randNormal(2, 100))) + 1  AS user_id,
    CASE floor(randUniform(1, 4))
        WHEN 1 THEN 'visits'
        WHEN 2 THEN 'views'
        ELSE 'clicks'
    END AS event_type,
    abs(round(randUniform(30, 100))) + 1  AS visits,
    abs(round(randUniform(20, 50))) + 1  AS views,
    abs(round(randUniform(1, 20))) + 1  AS clicks
FROM numbers(10000);