# LOCAL INSTALLATION & RUN
- git clone
- make init
- execute commands: 

```
sudo docker exec -it superset \
superset fab create-admin \
--username admin \
--firstname Superset \
--lastname Admin \
--email admin@admin.com \
--password password;
sudo docker exec -it superset superset db upgrade;
sudo docker exec -it superset superset init;
```
- on superset UI (localhost:8088) Settings -> Database Connections -> add database "Trino" with "trino://trino@host.docker.internal:8080" URI
and you can run sql query in SQL with multiple databases

```
SELECT * FROM postgresql.public.users u
LEFT JOIN clickhouse.default.stats s ON u.id = s.user_id
```