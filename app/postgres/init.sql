CREATE TABLE IF NOT EXISTS users(
    id      SERIAL PRIMARY KEY,
    name    VARCHAR
);

INSERT INTO users(name)
SELECT
    'User_' || generate_series(1, 100) as name;

CREATE TABLE IF NOT EXISTS purchases(
    id          SERIAL PRIMARY KEY,
    user_id     INTEGER REFERENCES public.users(id),
    order_name  VARCHAR,
    amount      NUMERIC(10,3)
);

INSERT INTO purchases(user_id, order_name, amount)
SELECT
	floor(random() * 100 + 1) as user_id,
 	CASE floor(random() * 3 + 1)
        WHEN 1 THEN 'order 1'
        WHEN 2 THEN 'order 2'
        ELSE 'order 3'
    END AS name,
    random() * 100 as amount
FROM
    generate_series(1, 1000);